package com.natife.timertrainer.ui.exercises

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.NumberPicker
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.natife.timertrainer.R
import com.natife.timertrainer.base.BaseActivity
import com.natife.timertrainer.data.model.ExerciseEntity
import com.natife.timertrainer.ui.main.MainViewModel
import com.natife.timertrainer.utils.Utils
import kotlinx.android.synthetic.main.fragment_new_exercise.*
import kotlinx.android.synthetic.main.toolbar.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel


/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [NewExerciseFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [NewExerciseFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class NewExerciseFragment : Fragment() {
    private val mainViewModel by sharedViewModel<MainViewModel>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        toolbar_back_button.setOnClickListener {
            findNavController().popBackStack()
        }
        toolbar_title.text = getString(R.string.new_exercise)
        setupOnClicks()

    }

    private fun setupOnClicks() {
        exercise_time.setOnClickListener { openTimePicker(it as TextView) }
        rest_time.setOnClickListener { openTimePicker(it as TextView) }
        loop.setOnClickListener { openPicker() }
        loop_minus.setOnClickListener {
            if (Integer.parseInt(loop.text.toString()) > 0) {
                loop.text = (Integer.parseInt(loop.text.toString()) - 1).toString()
            }
        }
        loop_plus.setOnClickListener {
            loop.text = (Integer.parseInt(loop.text.toString()) + 1).toString()
        }
        rest_minus.setOnClickListener {
            if (Utils.getMillisFromTime(rest_time.text.toString()) > 5000) {
                rest_time.text = Utils.getFormattedTime(Utils.getMillisFromTime(rest_time.text.toString()) - 5000)
            }
        }
        rest_plus.setOnClickListener {
            rest_time.text = Utils.getFormattedTime(Utils.getMillisFromTime(rest_time.text.toString()) + 5000)
        }
        exercise_minus.setOnClickListener {
            if (Utils.getMillisFromTime(exercise_time.text.toString()) > 5000) {
                exercise_time.text = Utils.getFormattedTime(Utils.getMillisFromTime(exercise_time.text.toString()) - 5000)
            }
        }
        exercise_plus.setOnClickListener {
            exercise_time.text = Utils.getFormattedTime(Utils.getMillisFromTime(exercise_time.text.toString()) + 5000)
        }
        button_add.setOnClickListener {
            if (new_exercise_name.text.length in 4..15) {
                if (Integer.parseInt(loop.text.toString()) > 0) {
                    val exerciseEntity = ExerciseEntity()
                    exerciseEntity.name = new_exercise_name.text.toString()
                    exerciseEntity.description = new_exercise_description.text.toString()
                    exerciseEntity.loops = Integer.parseInt(loop.text.toString())
                    exerciseEntity.restTime = Utils.getMillisFromTime(rest_time.text.toString())
                    exerciseEntity.exerciseTime = Utils.getMillisFromTime(exercise_time.text.toString())
                    mainViewModel.addExercise(exerciseEntity)
                    findNavController().popBackStack()
                } else {
                    (activity as BaseActivity).showErrorSnackbar(view!!, getString(R.string.loop_error))
                }
            } else new_exercise_name.error = getString(R.string.name_error)
        }
    }

    private fun openPicker() {
        val dialog = Dialog(context!!)
        val dialogView = layoutInflater.inflate(R.layout.dialog_number_picker, null)
        val pickerView = dialogView.findViewById<NumberPicker>(R.id.number_picker)
        pickerView.minValue = 1
        pickerView.maxValue = 50
        pickerView.setOnLongClickListener { true }
        pickerView.setOnClickListener {
            it as NumberPicker
            loop.text = it.value.toString()
            dialog.dismiss()
        }
        pickerView.wrapSelectorWheel = false
        dialog.setContentView(dialogView)
        dialog.show()
    }

    private fun openTimePicker(v: TextView) {
        val dialog = Dialog(context!!)
        val dialogView = layoutInflater.inflate(R.layout.dialog_number_picker, null)
        val pickerView = dialogView.findViewById<NumberPicker>(R.id.number_picker)
        pickerView.minValue = 1
        pickerView.maxValue = 720
        pickerView.setFormatter {
            Utils.getFormattedTimeForPicker(it)
        }
        pickerView.setOnLongClickListener { true }
        pickerView.setOnClickListener {
            it as NumberPicker
            v.text = Utils.getFormattedTimeForPicker(it.value)
            dialog.dismiss()
        }
        pickerView.wrapSelectorWheel = false
        dialog.setContentView(dialogView)
        dialog.show()

        try {
            val method = pickerView.javaClass.getDeclaredMethod("changeValueByOne", Boolean::class.javaPrimitiveType)
            method.isAccessible = true
            method.invoke(pickerView, true)
        } catch (e: Exception) {
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_new_exercise, container, false)
    }
}
