package com.natife.timertrainer.data

import android.util.Log
import com.natife.timertrainer.data.model.ExerciseEntity
import com.natife.timertrainer.data.model.TrainingEntity
import com.vicpin.krealmextensions.*
import io.realm.Sort


interface DataBase {
    fun insertTrainings(arrayList: ArrayList<TrainingEntity>)
    fun insertTraining(training: TrainingEntity)
    fun insertExercise(exercise: ExerciseEntity)
    fun getTrainingsList(): ArrayList<TrainingEntity>
    fun getTraining(id: String): TrainingEntity?
    fun getAllExercises(): ArrayList<ExerciseEntity>
    fun insertExerciseList(parsedExercises: List<ExerciseEntity>)
    fun removeTraining(id: String)
    fun removeExercise(name: String)
}

class DatabaseImpl : DataBase {
    override fun removeExercise(name: String) {
        ExerciseEntity().delete { equalTo("name", name) }
    }

    override fun insertExercise(exercise: ExerciseEntity) {
        exercise.save()
    }

    override fun removeTraining(id: String) {
        TrainingEntity().delete { equalTo("name", id) }
    }

    override fun insertExerciseList(parsedExercises: List<ExerciseEntity>) {
        parsedExercises.saveAll()
    }

    override fun getAllExercises(): ArrayList<ExerciseEntity> {
        return ArrayList(ExerciseEntity().querySorted("position", Sort.ASCENDING))
    }

    override fun getTraining(id: String): TrainingEntity? {
        return TrainingEntity().queryFirst { equalTo("name", id) }
    }

    override fun getTrainingsList(): ArrayList<TrainingEntity> {
        return ArrayList(TrainingEntity().querySorted("position", Sort.ASCENDING))
    }

    override fun insertTrainings(arrayList: ArrayList<TrainingEntity>) {
        arrayList.saveAll()
    }

    override fun insertTraining(training: TrainingEntity) {
        Log.e("training", training.name)
        training.save()
    }
}