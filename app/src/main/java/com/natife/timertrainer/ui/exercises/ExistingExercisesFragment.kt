package com.natife.timertrainer.ui.exercises

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.natife.timertrainer.R
import com.natife.timertrainer.base.BaseActivity
import com.natife.timertrainer.data.model.ExerciseEntity
import com.natife.timertrainer.ui.custom.SimpleItemTouchHelperCallback
import com.natife.timertrainer.ui.main.MainViewModel
import kotlinx.android.synthetic.main.fragment_existing_exercises.*
import kotlinx.android.synthetic.main.toolbar.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel


/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [ExistingExercisesFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [ExistingExercisesFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class ExistingExercisesFragment : Fragment(), ExistingExerciseAdapter.OnCharacteristicClickListener, ExistingExerciseAdapter.OnListChanged {
    private val mainViewModel by sharedViewModel<MainViewModel>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_existing_exercises, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        toolbar_title.text = getString(R.string.available_exercises)
        mainViewModel.loadExercises()
        toolbar_plus_button.visibility = View.VISIBLE
        toolbar_back_button.setOnClickListener { findNavController().popBackStack() }
        toolbar_plus_button.setOnClickListener { findNavController().navigate(R.id.action_existingExercisesFragment_to_newExerciseFragment) }
        exercise_list.layoutManager = LinearLayoutManager(context)
        exercise_list.adapter = ExistingExerciseAdapter(mainViewModel.exercisesList, this, this, false)
        val callback = SimpleItemTouchHelperCallback(exercise_list.adapter as ExistingExerciseAdapter)
        val touchHelper = ItemTouchHelper(callback)
        touchHelper.attachToRecyclerView(exercise_list)
        select.setOnClickListener {
            val checked = (exercise_list.adapter as ExistingExerciseAdapter).checkedArray
            if (!checked.isEmpty()) {
                for (exercise in mainViewModel.exercisesList) {
                    if (checked.contains(exercise.name)) {
                        mainViewModel.addExerciseToTempTraining(exercise)
                    }
                }
                findNavController().popBackStack()
            } else (activity as BaseActivity).showErrorSnackbar(view, getString(R.string.error_select_more))
        }
    }

    override fun onItemRemoved(id: Int, name: String) {
        showSnackbar(name)
    }

    private fun showSnackbar(id: String) {
        Snackbar.make(view!!, getString(R.string.confirmation), Snackbar.LENGTH_LONG)
                .setAction(R.string.remove) {
                    mainViewModel.removeExercise(id)
                    mainViewModel.loadExercises()
                    (exercise_list.adapter as ExistingExerciseAdapter).exercises = mainViewModel.exercisesList
                    (exercise_list.adapter as ExistingExerciseAdapter).notifyDataSetChanged()
                }.show()
    }

    override fun onItemMoved(fromPosition: Int, toPosition: Int) {
        mainViewModel.saveSortedExercises((exercise_list.adapter as ExistingExerciseAdapter).exercises)
    }

    override fun onClick(id: Int?, exerciseEntity: ExerciseEntity) {
        mainViewModel.addExerciseToTempTraining(exerciseEntity)
        findNavController().popBackStack()
    }

}
