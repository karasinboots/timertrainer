package com.natife.timertrainer.ui.homefragment

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.natife.timertrainer.R
import com.natife.timertrainer.data.model.TrainingEntity
import com.natife.timertrainer.ui.custom.ItemTouchHelperAdapter
import kotlinx.android.synthetic.main.item_home_list.view.*
import java.util.*


class HomeAdapter(var training: ArrayList<TrainingEntity>, private val listener: OnCharacteristicClickListener, private val onListChanged: OnListChanged, private val onMoreClickedListener: OnMoreClickedListener) :
        RecyclerView.Adapter<HomeAdapter.ViewHolder>(),
        ItemTouchHelperAdapter {

    override fun onItemMove(fromPosition: Int, toPosition: Int) {
        if (fromPosition < toPosition) {
            for (i in fromPosition until toPosition) {
                Collections.swap(training, i, i + 1)
                val tempPosition = training[fromPosition].position
                training[fromPosition].position = training[toPosition].position
                training[toPosition].position = tempPosition
            }
        } else {
            for (i in fromPosition downTo toPosition + 1) {
                Collections.swap(training, i, i - 1)
                val tempPosition = training[fromPosition].position
                training[fromPosition].position = training[toPosition].position
                training[toPosition].position = tempPosition
            }
        }
        onListChanged.onItemMoved(fromPosition, toPosition)
        notifyItemMoved(fromPosition, toPosition)
    }

    override fun onItemDismiss(position: Int) {
        if (training[position].type == 0) {
            notifyDataSetChanged()
            return
        }
        onListChanged.onItemRemoved(training[position].name)
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val training = training[position]
        training.position = position
        if (training.type == 0) {
            holder.ivMore.visibility = View.GONE
        } else {
            holder.ivMore.visibility = View.VISIBLE
        }
        holder.tvTitle.text = training.name
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_home_list, parent, false))
    }

    override fun getItemCount(): Int {
        return training.size
    }

    interface OnCharacteristicClickListener {
        fun onClick(id: Int?, trainingEntity: TrainingEntity)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

        val tvTitle: TextView = itemView.textView
        val ivMore = itemView.more!!

        init {
            ivMore.setOnClickListener(this)
            itemView.setOnClickListener(this)
        }

        override fun onClick(v: View) {
            when (v.id) {
                R.id.parent -> {
                    listener.onClick(layoutPosition, training[layoutPosition])
                }
                R.id.more -> {
                    onMoreClickedListener.onMoreClicked(v, training[layoutPosition].name)
                }
            }
        }
    }

    interface OnListChanged {
        fun onItemRemoved(id: String)
        fun onItemMoved(fromPosition: Int, toPosition: Int)
    }

    interface OnMoreClickedListener {
        fun onMoreClicked(v: View, id: String)
    }


}