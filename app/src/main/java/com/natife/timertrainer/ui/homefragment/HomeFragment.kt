package com.natife.timertrainer.ui.homefragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.widget.PopupMenu
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.snackbar.Snackbar
import com.natife.timertrainer.R
import com.natife.timertrainer.data.model.TrainingEntity
import com.natife.timertrainer.service.TimerService
import com.natife.timertrainer.ui.custom.SimpleItemTouchHelperCallback
import com.natife.timertrainer.ui.main.MainViewModel
import com.natife.timertrainer.utils.Utils
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.toolbar.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [HomeFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [HomeFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class HomeFragment : Fragment(), HomeAdapter.OnCharacteristicClickListener, View.OnClickListener, HomeAdapter.OnListChanged, HomeAdapter.OnMoreClickedListener, PopupMenu.OnMenuItemClickListener {
    private val homeViewModel by viewModel<HomeViewModel>()
    private val mainViewModel by sharedViewModel<MainViewModel>()

    private lateinit var mBottomSheetDialog: BottomSheetDialog
    private lateinit var sheetView: View


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        toolbar_title.text = getString(R.string.trainings_title)
        toolbar_back_button.visibility = View.GONE
        toolbar_info.visibility = View.VISIBLE
        toolbar_plus_button.visibility = View.VISIBLE
        toolbar_exit.visibility = View.VISIBLE
        toolbar_exit.setOnClickListener {
            activity!!.stopService(Intent(activity, TimerService::class.java))
            System.exit(0)
        }
        toolbar_info.setOnClickListener { findNavController().navigate(R.id.action_homeFragment_to_aboutFragment) }
        toolbar_back_button.setOnClickListener { findNavController().popBackStack() }
        toolbar_plus_button.setOnClickListener {
            mBottomSheetDialog = BottomSheetDialog(activity!!)
            sheetView = activity!!.layoutInflater.inflate(R.layout.bottom_sheet_dialog_view, null)
            mBottomSheetDialog.setContentView(sheetView)
            mBottomSheetDialog.show()
            sheetView.findViewById<Button>(R.id.cancel).setOnClickListener(this)
            sheetView.findViewById<Button>(R.id.add).setOnClickListener(this)
            sheetView.findViewById<Button>(R.id.add).imeOptions = EditorInfo.IME_ACTION_DONE
        }
        home_list.layoutManager = LinearLayoutManager(context)
        home_list.adapter = HomeAdapter(mainViewModel.loadTrainings(), this, this, this)
        val callback = SimpleItemTouchHelperCallback(home_list.adapter as HomeAdapter)
        val touchHelper = ItemTouchHelper(callback)
        touchHelper.attachToRecyclerView(home_list)
    }

    override fun onItemRemoved(id: String) {
        showDeleteSnackbar(id)
    }

    private fun showDeleteSnackbar(id: String) {
        Snackbar.make(view!!, getString(R.string.confirmation), Snackbar.LENGTH_LONG)
                .setAction(R.string.remove) {
                    mainViewModel.removeTraining(id)
                    (home_list.adapter as HomeAdapter).training = mainViewModel.loadTrainings()
                    (home_list.adapter as HomeAdapter).notifyDataSetChanged()
                }.show()
    }

    private fun showClearBottomSheet(trainingEntity: TrainingEntity) {
        val sheetView = activity!!.layoutInflater.inflate(R.layout.bottom_sheet_reset_training, null)
        val bottomSheetDialog = BottomSheetDialog(activity!!)
        sheetView.findViewById<Button>(R.id.reset).setOnClickListener {
            resetTraining(trainingEntity)
            bottomSheetDialog.dismiss()
        }
        sheetView.findViewById<Button>(R.id.dismiss).setOnClickListener {
            bottomSheetDialog.dismiss()
            findNavController().navigate(R.id.action_homeFragment_to_timerFragment)
        }
        bottomSheetDialog.setContentView(sheetView)
        bottomSheetDialog.show()
    }

    private fun resetTraining(trainingEntity: TrainingEntity) {
        mainViewModel.timerService?.also {
            it.cancelTraining()
            it.trainingEntity = trainingEntity
            it.init()
            it.makeWaitNotification()
            findNavController().navigate(R.id.action_homeFragment_to_timerFragment)
        }
    }

    override fun onItemMoved(fromPosition: Int, toPosition: Int) {
        mainViewModel.saveSortedTrainings((home_list.adapter as HomeAdapter).training)
    }

    override fun onMoreClicked(v: View, id: String) {
        showPopup(v, id)
    }

    private var clickedId: String = ""
    private fun showPopup(v: View, id: String) {
        clickedId = id
        val popup = PopupMenu(context!!, v)
        popup.setOnMenuItemClickListener(this)
        popup.inflate(R.menu.more)
        popup.show()
    }

    override fun onMenuItemClick(item: MenuItem?): Boolean {
        item?.also {
            when (it.itemId) {
                R.id.action_edit -> {
                    val bundle = Bundle()
                    for (training in mainViewModel.loadTrainings()) {
                        if (training.name == clickedId) {
                            bundle.putString(Utils.NEW_TRAINING_NAME, training.name)
                            bundle.putString(Utils.NEW_TRAINING_DESCRIPTION, training.description)
                            bundle.putBoolean(Utils.EDIT_TRAINING, true)
                            findNavController().navigate(R.id.action_homeFragment_to_newTrainingFragment, bundle)
                        }
                    }
                }
                R.id.action_remove -> {
                    showDeleteSnackbar(clickedId)
                }
            }
        }
        return true
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.add -> {
                val name = sheetView.findViewById<EditText>(R.id.new_training_name)
                val description = sheetView.findViewById<EditText>(R.id.new_training_description)
                if (name.text.length in 4..15)
                    if (homeViewModel.isNameAvailable(name.text.toString())) {
                        val bundle = Bundle()
                        bundle.putString(Utils.NEW_TRAINING_NAME, name.text.toString())
                        bundle.putString(Utils.NEW_TRAINING_DESCRIPTION, description.text.toString())
                        bundle.putBoolean(Utils.EDIT_TRAINING, false)
                        mBottomSheetDialog.dismiss()
                        mainViewModel.editMode = false
                        findNavController().navigate(R.id.action_homeFragment_to_newTrainingFragment, bundle)
                    } else name.error = getString(R.string.already_exist)
                else name.error = getString(R.string.name_error)
            }
            R.id.cancel -> mBottomSheetDialog.dismiss()
        }
    }

    override fun onClick(id: Int?, trainingEntity: TrainingEntity) {
        mainViewModel.timerService?.also { service ->
            service.trainingEntity?.also {
                if (it.name == trainingEntity.name
                        && service.isTraining) {
                    findNavController().navigate(R.id.action_homeFragment_to_timerFragment)
                    return
                }
            }
            if (service.isTraining) {
                showClearBottomSheet(trainingEntity)
            } else {
                resetTraining(trainingEntity)
            }
        }
    }
}
