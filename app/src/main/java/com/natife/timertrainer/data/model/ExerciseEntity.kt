package com.natife.timertrainer.data.model

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass

@RealmClass
open class ExerciseEntity : RealmObject() {
    @PrimaryKey
    var name: String = ""
    var description: String = ""
    var exerciseTime: Long = 0
    var restTime: Long = 0
    var loops: Int = 0
    var position: Int = 0
    var type: Int = 1
}