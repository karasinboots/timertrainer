package com.natife.timertrainer.utils

import java.text.SimpleDateFormat
import java.util.*

class Utils {
    companion object {
        const val NEW_TRAINING_NAME = "newTrainingName"
        const val NEW_TRAINING_DESCRIPTION = "newTrainingDescription"
        const val EDIT_TRAINING = "editTraining"

        fun getFormattedTime(millis: Long): String {
            val formatter = SimpleDateFormat("mm:ss", Locale.getDefault())
            formatter.timeZone = TimeZone.getTimeZone("GMT")
            return formatter.format(Date(millis))
        }

        fun getFormattedTimeForPicker(number: Int): String {
            val formatter = SimpleDateFormat("mm:ss", Locale.getDefault())
            formatter.timeZone = TimeZone.getTimeZone("GMT")
            return formatter.format(Date(number * 5000L))
        }

        fun getMillisFromTime(time: String): Long {
            val timeSplit = time.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            return (Integer.parseInt(timeSplit[0]) * 60 + Integer.parseInt(timeSplit[1])) * 1000L
        }

        fun getProgressInPercents(max: Long, current: Long): Int {
            return ((max - current).toFloat() / max.toFloat() * 100).toInt()
        }
    }
}