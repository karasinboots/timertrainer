package com.natife.timertrainer

import android.app.Application
import android.content.Intent
import android.util.Log
import com.natife.timertrainer.di.moduleList
import com.natife.timertrainer.service.TimerService
import io.realm.Realm
import io.realm.RealmConfiguration
import org.koin.android.ext.android.startKoin
import timber.log.Timber
import java.net.ConnectException

class MyApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        initTimber()
        startKoin(this, moduleList)
        Realm.init(this)
        val config = RealmConfiguration
                .Builder()
                .deleteRealmIfMigrationNeeded()
                .name("libraryRealm")
                .build()
        Realm.setDefaultConfiguration(config)
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            this.startForegroundService(Intent(this, TimerService::class.java))
        }
    }

    private fun initTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        } else {
            Timber.plant(object : Timber.Tree() {
                override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {
                    if (priority == Log.VERBOSE || priority == Log.DEBUG) {
                        return
                    }
                    if (t != null && t !is ConnectException) {
                        try {
//                            Crashlytics.logException(t)
//                            FirebaseCrash.report(t)
                        } catch (ignore: Exception) {
                            //ignore
                        }
                    }
                }
            })
        }
    }
}