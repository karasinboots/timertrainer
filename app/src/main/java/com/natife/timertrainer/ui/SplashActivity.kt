package com.natife.timertrainer.ui

import android.content.Intent
import android.os.Bundle
import android.view.animation.Animation
import android.view.animation.Animation.AnimationListener
import com.natife.timertrainer.R
import com.natife.timertrainer.base.BaseActivity
import com.natife.timertrainer.ui.custom.ProgressBarAnimation
import com.natife.timertrainer.ui.main.MainActivity
import com.natife.timertrainer.ui.main.MainViewModel
import kotlinx.android.synthetic.main.activity_splash.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class SplashActivity : BaseActivity(), AnimationListener {
    private val mainViewModel by viewModel<MainViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        mainViewModel.savePredefinedTrainings()
        mainViewModel.savePredefinedExercises()

        val animation = ProgressBarAnimation(progressBar, 0f, 3000f)
        animation.duration = 300
        progressBar.startAnimation(animation)
        progressBar.animation.setAnimationListener(this)
    }

    override fun onAnimationRepeat(animation: Animation?) {

    }

    override fun onAnimationStart(animation: Animation?) {

    }

    override fun onAnimationEnd(animation: Animation?) {
        startActivity(Intent(applicationContext, MainActivity::class.java))
        finish()
    }

    override fun onBackPressed() {

    }
}