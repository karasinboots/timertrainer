package com.natife.timertrainer.base

import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar

abstract class BaseActivity : AppCompatActivity() {
    fun showErrorSnackbar(v: View, error: String) {
        Snackbar.make(v, error, Snackbar.LENGTH_LONG).show()
    }
}