package com.natife.timertrainer.ui.main

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.IBinder
import androidx.navigation.findNavController
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.natife.timertrainer.R
import com.natife.timertrainer.base.BaseActivity
import com.natife.timertrainer.service.TimerService
import org.koin.androidx.viewmodel.ext.android.viewModel


class MainActivity : BaseActivity() {

    private val mainViewModel by viewModel<MainViewModel>()

    private var sConn = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName, binder: IBinder) {
            timerService = (binder as TimerService.MyBinder).service
            mainViewModel.timerService = timerService
            intent.extras?.getInt("extra")?.also { navPath ->
                timerService?.also {
                    if (it.isTraining) {
                        try {
                            findNavController(R.id.homeFragment).navigate(navPath)
                        } catch (e: Exception) {

                        }
                        intent?.extras?.remove("extra")
                    }
                }
            }
        }

        override fun onServiceDisconnected(name: ComponentName) {
            timerService = null
        }
    }

    var timerService: TimerService? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        lifecycle.addObserver(mainViewModel)
        bindService(Intent(this, TimerService::class.java), sConn, Context.BIND_AUTO_CREATE)
        prepareAd()
    }

    override fun onDestroy() {
        super.onDestroy()
        unbindService(sConn)
    }

    private fun prepareAd() {
        val adView: AdView = this.findViewById(R.id.adView)
        val adRequest = AdRequest.Builder()
                .build()
        adView.loadAd(adRequest)
        adView.adListener = object : AdListener() {
            override fun onAdLoaded() {
            }

            override fun onAdFailedToLoad(errorCode: Int) {
            }

            override fun onAdOpened() {
            }

            override fun onAdLeftApplication() {
                adView.destroy()
            }

            override fun onAdClosed() {
            }
        }
    }
}
