package com.natife.timertrainer.ui.timerfragment

import com.natife.timertrainer.base.BaseViewModel
import com.natife.timertrainer.utils.PreferenceHandler


class TimerViewModel(private val preferenceHandler: PreferenceHandler) : BaseViewModel() {
    fun setAudioEnabled(isEnabled: Boolean) {
        preferenceHandler.setAudioEnabled(isEnabled)
    }

    fun setVibrationEnabled(isEnabled: Boolean) {
        preferenceHandler.setVibrationEnabled(isEnabled)
    }

    fun setSkipPauseEnabled(isEnabled: Boolean) {
        preferenceHandler.setSkipPause(isEnabled)
    }

    fun isAudioEnabled(): Boolean {
        return preferenceHandler.isAudioEnabled()
    }

    fun isVibrationEnabled(): Boolean {
        return preferenceHandler.isVibrationEnabled()
    }

    fun isSkippingPauseEnabled(): Boolean {
        return preferenceHandler.isSkipPause()
    }
}