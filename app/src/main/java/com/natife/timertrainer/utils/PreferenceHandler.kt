package com.natife.timertrainer.utils

import android.content.SharedPreferences

interface PreferenceHandler {
    fun setVibrationEnabled(isEnabled: Boolean)
    fun setAudioEnabled(isEnabled: Boolean)
    fun setSkipPause(skip: Boolean)
    fun isVibrationEnabled(): Boolean
    fun isAudioEnabled(): Boolean
    fun isSkipPause(): Boolean

}

class PreferenceHandlerImpl(private val sharedPreferences: SharedPreferences) : PreferenceHandler {
    companion object {
        private const val VIBRATION_ENABLED = "vibration"
        private const val AUDIO_ENABLED = "audio"
        private const val SKIP_PAUSE = "skip_pause"
    }

    override fun setVibrationEnabled(isEnabled: Boolean) {
        sharedPreferences.edit().putBoolean(VIBRATION_ENABLED, isEnabled).commit()
    }

    override fun setAudioEnabled(isEnabled: Boolean) {
        sharedPreferences.edit().putBoolean(AUDIO_ENABLED, isEnabled).commit()
    }

    override fun setSkipPause(skip: Boolean) {
        sharedPreferences.edit().putBoolean(SKIP_PAUSE, skip).commit()
    }

    override fun isVibrationEnabled(): Boolean {
        return sharedPreferences.getBoolean(VIBRATION_ENABLED, true)
    }

    override fun isAudioEnabled(): Boolean {
        return sharedPreferences.getBoolean(AUDIO_ENABLED, true)
    }

    override fun isSkipPause(): Boolean {
        return sharedPreferences.getBoolean(SKIP_PAUSE, true)
    }
}