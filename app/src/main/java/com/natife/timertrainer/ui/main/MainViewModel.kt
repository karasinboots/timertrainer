package com.natife.timertrainer.ui.main

import com.natife.timertrainer.base.BaseViewModel
import com.natife.timertrainer.data.DataManager
import com.natife.timertrainer.data.model.ExerciseEntity
import com.natife.timertrainer.data.model.TrainingEntity
import com.natife.timertrainer.service.TimerService
import io.realm.RealmList

class MainViewModel constructor(private val dataManager: DataManager) : BaseViewModel() {

    private var tempTrainingEntity: TrainingEntity = TrainingEntity()
    var exercisesList = ArrayList<ExerciseEntity>()
    var editMode = false
    var currentTrainingId = ""
    var timerService: TimerService? = null

    fun savePredefinedTrainings() {
        dataManager.parsePredefinedTrainings()
    }

    fun loadTraining(name: String) {
        dataManager.loadTraining(name)?.also {
            tempTrainingEntity = it
        }
    }

    fun loadTrainings(): ArrayList<TrainingEntity> {
        return dataManager.loadTrainingsList()
    }

    fun createTempTraining(name: String) {
        tempTrainingEntity.name = name
    }

    fun addExerciseToTempTraining(exercise: ExerciseEntity) {
        tempTrainingEntity.exercises.add(exercise)
    }

    fun getExercisesFromTempTraining(): List<ExerciseEntity> {
        return tempTrainingEntity.exercises
    }

    fun loadExercises() {
        dataManager.loadExercises()?.also {
            exercisesList = it
        }
    }

    fun clearTempTraining() {
        tempTrainingEntity.name = ""
        tempTrainingEntity.exercises.clear()
    }

    fun removeTraining(name: String) {
        dataManager.removeTraining(name)
    }

    fun removeExercise(name: String) {
        dataManager.removeExercise(name)
    }

    fun saveTempTraining() {
        dataManager.addTraining(tempTrainingEntity)
        clearTempTraining()
    }

    fun removeItemFromTempTraining(id: Int) {
        tempTrainingEntity.exercises.removeAt(id)
    }

    fun saveSortedListToTempTraining(sortedExercises: List<ExerciseEntity>) {
        tempTrainingEntity.exercises = sortedExercises as RealmList
    }

    fun savePredefinedExercises() {
        dataManager.parsePredefinedExercises()
    }

    fun saveSortedTrainings(trainingList: ArrayList<TrainingEntity>) {
        dataManager.addTrainings(trainingList)
    }

    fun addDescriptionToTempTraining(it: String) {
        tempTrainingEntity.description = it
    }

    fun addExercise(exercise: ExerciseEntity) {
        dataManager.addExercise(exercise)
    }

    fun saveSortedExercises(exercises: List<ExerciseEntity>) {
        dataManager.addExercises(exercises)
    }
}