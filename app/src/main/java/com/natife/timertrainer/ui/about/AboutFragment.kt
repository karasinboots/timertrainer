package com.natife.timertrainer.ui.about

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.natife.timertrainer.BuildConfig
import com.natife.timertrainer.R
import kotlinx.android.synthetic.main.fragment_about.*
import kotlinx.android.synthetic.main.toolbar.*


class AboutFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_about, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        version.text = getString(R.string.version, BuildConfig.VERSION_NAME)
        toolbar_back_button.setOnClickListener {
            findNavController().popBackStack()
        }
        toolbar_title.text = getString(R.string.about_title)
        link.setOnClickListener {
            val url = "http://www.natife.com"
            val i = Intent(Intent.ACTION_VIEW)
            i.data = Uri.parse(url)
            if (i.resolveActivity(context!!.packageManager) != null)
                startActivity(i)
        }
    }
}