package com.natife.timertrainer.data

import android.content.Context
import com.natife.timertrainer.data.model.ExerciseEntity
import com.natife.timertrainer.data.model.TrainingEntity
import com.natife.timertrainer.utils.GsonParser

interface DataManager {
    fun loadTrainingsList(): ArrayList<TrainingEntity>
    fun loadTraining(id: String): TrainingEntity?
    fun addTraining(trainingEntity: TrainingEntity)
    fun addExercise(exercise: ExerciseEntity)
    fun removeTraining(id: String)
    fun parsePredefinedTrainings()
    fun parsePredefinedExercises()
    fun checkNameAvailability(name: String): Boolean
    fun loadExercises(): ArrayList<ExerciseEntity>?
    fun addTrainings(trainingList: ArrayList<TrainingEntity>)
    fun removeExercise(name: String)
    fun addExercises(exercises: List<ExerciseEntity>)
}

class DataManagerImpl constructor(private val database: DataBase, private val context: Context) : DataManager {
    override fun addExercises(exercises: List<ExerciseEntity>) {
        database.insertExerciseList(exercises)
    }

    override fun removeExercise(name: String) {
        database.removeExercise(name)
    }

    override fun addExercise(exercise: ExerciseEntity) {
        exercise.position = database.getAllExercises().size
        database.insertExercise(exercise)
    }

    override fun addTrainings(trainingList: ArrayList<TrainingEntity>) {
        database.insertTrainings(trainingList)
    }

    override fun loadExercises(): ArrayList<ExerciseEntity>? {
        return database.getAllExercises()
    }

    override fun checkNameAvailability(name: String): Boolean {
        val trainings = database.getTrainingsList()
        for (trainingEntity in trainings) {
            if (trainingEntity.name.toLowerCase() == name.toLowerCase()) return false
        }
        return true
    }

    override fun parsePredefinedTrainings() {
        if (database.getTrainingsList().isEmpty())
            database.insertTrainings(GsonParser.parseTrainings(context))
    }

    override fun parsePredefinedExercises() {
        if (database.getAllExercises().isEmpty())
            database.insertExerciseList(GsonParser.parseExercises(context))
    }

    override fun loadTrainingsList(): ArrayList<TrainingEntity> {
        return database.getTrainingsList()
    }

    override fun loadTraining(id: String): TrainingEntity? {
        return database.getTraining(id)
    }

    override fun addTraining(trainingEntity: TrainingEntity) {
        trainingEntity.position = database.getTrainingsList().size
        database.insertTraining(trainingEntity)
    }

    override fun removeTraining(id: String) {
        database.removeTraining(id)
    }
}
