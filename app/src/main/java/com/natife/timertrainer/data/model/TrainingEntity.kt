package com.natife.timertrainer.data.model

import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass


@RealmClass
open class TrainingEntity : RealmObject() {
    @PrimaryKey
    var name: String = ""
    var description: String = ""
    var position: Int = 0
    var type: Int = 1
    var exercises: RealmList<ExerciseEntity> = RealmList()
}