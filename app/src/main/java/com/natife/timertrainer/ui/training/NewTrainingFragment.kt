package com.natife.timertrainer.ui.training

import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import com.natife.timertrainer.R
import com.natife.timertrainer.base.BaseActivity
import com.natife.timertrainer.data.model.ExerciseEntity
import com.natife.timertrainer.ui.custom.SimpleItemTouchHelperCallback
import com.natife.timertrainer.ui.exercises.ExistingExerciseAdapter
import com.natife.timertrainer.ui.main.MainViewModel
import com.natife.timertrainer.utils.Utils
import kotlinx.android.synthetic.main.fragment_new_training.*
import kotlinx.android.synthetic.main.toolbar.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel


/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [NewTrainingFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [NewTrainingFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class NewTrainingFragment : Fragment(), View.OnClickListener, ExistingExerciseAdapter.OnCharacteristicClickListener, ExistingExerciseAdapter.OnListChanged {
    private val mainViewModel by sharedViewModel<MainViewModel>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_new_training, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.getString(Utils.NEW_TRAINING_NAME)?.also { it ->
            mainViewModel.currentTrainingId = it
            toolbar_title.text = mainViewModel.currentTrainingId
            toolbar_plus_button.visibility = View.VISIBLE
            arguments?.getString(Utils.NEW_TRAINING_DESCRIPTION)?.also {
                mainViewModel.addDescriptionToTempTraining(it)
            }
        }
        temp_training_list.layoutManager = LinearLayoutManager(context)
        arguments?.getBoolean(Utils.EDIT_TRAINING)?.also { edit ->
            if (edit) {
                if (!mainViewModel.editMode) {
                    mainViewModel.loadTraining(mainViewModel.currentTrainingId)
                    mainViewModel.editMode = true
                }
                add2.text = getString(R.string.save)
                temp_training_list.adapter = ExistingExerciseAdapter(mainViewModel.getExercisesFromTempTraining(),
                        this, this, true)
            } else {
                mainViewModel.createTempTraining(mainViewModel.currentTrainingId)
                temp_training_list.adapter = ExistingExerciseAdapter(mainViewModel.getExercisesFromTempTraining(),
                        this, this, true)
            }
            val callback = SimpleItemTouchHelperCallback(temp_training_list.adapter as ExistingExerciseAdapter)
            val touchHelper = ItemTouchHelper(callback)
            touchHelper.attachToRecyclerView(temp_training_list)
            checkIsListEmpty()
        }
        setupOnClicks()
    }

    private fun setupOnClicks() {
        view?.also {
            it.isFocusableInTouchMode = true
            it.requestFocus()
            it.setOnKeyListener { v, keyCode, event ->
                when (keyCode) {
                    KeyEvent.KEYCODE_BACK -> {
                        clearTempAndExitFromEditMode()
                        true
                    }
                    else -> true
                }
            }
        }

        cancel2.setOnClickListener {
            clearTempAndExitFromEditMode()
        }

        add2.setOnClickListener {
            mainViewModel.editMode = false
            mainViewModel.saveTempTraining()
            findNavController().popBackStack()
        }

        toolbar_back_button.setOnClickListener {
            clearTempAndExitFromEditMode()
        }

        toolbar_plus_button.setOnClickListener(this)
    }

    private fun clearTempAndExitFromEditMode() {
        mainViewModel.editMode = false
        mainViewModel.clearTempTraining()
        findNavController().popBackStack()
    }


    private fun checkIsListEmpty() {
        if (mainViewModel.getExercisesFromTempTraining().isNotEmpty()) {
            no_exercises.visibility = View.GONE
            add2.isEnabled = true
        } else {
            no_exercises.visibility = View.VISIBLE
            add2.isEnabled = false
        }
    }

    override fun onItemRemoved(id: Int, name: String) {
        if (mainViewModel.editMode && mainViewModel.getExercisesFromTempTraining().size == 1) {
            (activity as BaseActivity).showErrorSnackbar(view!!, getString(R.string.error_last_exercise))
            return
        }
        mainViewModel.removeItemFromTempTraining(id)
        checkIsListEmpty()
    }

    override fun onItemMoved(fromPosition: Int, toPosition: Int) {
        mainViewModel.saveSortedListToTempTraining((temp_training_list.adapter as ExistingExerciseAdapter).exercises)
    }

    override fun onClick(v: View?) {
        findNavController().navigate(R.id.action_newTrainingFragment_to_existingExercisesFragment)
    }

    override fun onClick(id: Int?, exerciseEntity: ExerciseEntity) {

    }
}
