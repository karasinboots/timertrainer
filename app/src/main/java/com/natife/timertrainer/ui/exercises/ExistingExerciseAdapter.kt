package com.natife.timertrainer.ui.exercises

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.CompoundButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.natife.timertrainer.R
import com.natife.timertrainer.data.model.ExerciseEntity
import com.natife.timertrainer.ui.custom.ItemTouchHelperAdapter
import com.natife.timertrainer.utils.Utils
import kotlinx.android.synthetic.main.item_exercise_list.view.*
import java.util.*
import kotlin.collections.HashSet


class ExistingExerciseAdapter(var exercises: List<ExerciseEntity>, private val onListChanged: OnListChanged, private val listener: OnCharacteristicClickListener, private val canRemovePredefined: Boolean) :
        RecyclerView.Adapter<ExistingExerciseAdapter.ViewHolder>(),
        ItemTouchHelperAdapter {

    val checkedArray: HashSet<String> = HashSet()

    override fun onItemMove(fromPosition: Int, toPosition: Int) {
        if (fromPosition < toPosition) {
            for (i in fromPosition until toPosition) {
                Collections.swap(exercises, i, i + 1)
                val tempPosition = exercises[fromPosition].position
                exercises[fromPosition].position = exercises[toPosition].position
                exercises[toPosition].position = tempPosition
            }
        } else {
            for (i in fromPosition downTo toPosition + 1) {
                Collections.swap(exercises, i, i - 1)
                val tempPosition = exercises[fromPosition].position
                exercises[fromPosition].position = exercises[toPosition].position
                exercises[toPosition].position = tempPosition
            }
        }
        notifyItemMoved(fromPosition, toPosition)
        onListChanged.onItemMoved(fromPosition, toPosition)
    }

    override fun onItemDismiss(position: Int) {
        if (exercises[position].type != 0 || canRemovePredefined) {
            onListChanged.onItemRemoved(position, exercises[position].name)
            notifyDataSetChanged()
        } else {
            notifyDataSetChanged()
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val exercise = exercises[position]
        var isChecked = false
        exercise.position = position
        if (canRemovePredefined) holder.checkBox.visibility = View.GONE
        if (checkedArray.contains(exercise.name)) {
            isChecked = true
        }
        holder.checkBox.isChecked = isChecked
        holder.checkBox.setOnCheckedChangeListener { _: CompoundButton, b: Boolean ->
            if (b) {
                checkedArray.add(exercises[position].name)
            } else {
                checkedArray.remove(exercises[position].name)
            }
        }
        holder.tvTitle.text = exercise.name
        holder.tvTime.text = holder.tvTime.context.getString(R.string.exercise_time, Utils.getFormattedTime(exercise.exerciseTime), Utils.getFormattedTime(exercise.restTime))
        holder.tvRepeat.text = holder.tvTime.context.getString(R.string.exercise_loops, exercise.loops)
    }

    override fun onViewRecycled(holder: ViewHolder) {
        super.onViewRecycled(holder)
        holder.checkBox.setOnCheckedChangeListener(null)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_exercise_list, parent, false))
    }

    override fun getItemCount(): Int {
        return exercises.size
    }

    interface OnCharacteristicClickListener {
        fun onClick(id: Int?, exerciseEntity: ExerciseEntity)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        val checkBox: CheckBox = itemView.selected
        val tvTitle: TextView = itemView.exercise_name
        val tvTime: TextView = itemView.exercise_time_text
        val tvRepeat: TextView = itemView.exercise_repeat

        init {
            itemView.setOnClickListener(this)
        }

        override fun onClick(v: View) {
            listener.onClick(layoutPosition, exercises[layoutPosition])
        }
    }

    interface OnListChanged {
        fun onItemRemoved(id: Int, name: String)
        fun onItemMoved(fromPosition: Int, toPosition: Int)
    }
}