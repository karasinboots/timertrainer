package com.natife.timertrainer.ui.homefragment

import com.natife.timertrainer.base.BaseViewModel
import com.natife.timertrainer.data.DataManager

class HomeViewModel constructor(private val dataManager: DataManager) : BaseViewModel() {
    fun isNameAvailable(name: String): Boolean {
        return dataManager.checkNameAvailability(name)
    }
}