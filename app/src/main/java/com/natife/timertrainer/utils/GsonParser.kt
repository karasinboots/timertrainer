package com.natife.timertrainer.utils

import android.content.Context
import android.util.Log
import com.google.gson.GsonBuilder
import com.natife.timertrainer.data.model.ExerciseEntity
import com.natife.timertrainer.data.model.TrainingEntity


class GsonParser {

    companion object {

        private val availableTrainings = mutableSetOf(
                "eyes_one.json",
                "eyes_two.json",
                "neck_one.json",
                "neck_two.json"
        )

        fun parseTrainings(context: Context): ArrayList<TrainingEntity> {
            val trainings = ArrayList<TrainingEntity>()
            val gson = GsonBuilder().create()
            for (key in availableTrainings) {
                val json = assetJSONFile(key, context)
                val training = gson.fromJson(json, TrainingEntity::class.java)
                trainings.add(training)
            }
            return trainings
        }

        fun parseExercises(context: Context): ArrayList<ExerciseEntity> {
            val exercises = ArrayList<ExerciseEntity>()
            val gson = GsonBuilder().create()
            for (key in availableTrainings) {
                val json = assetJSONFile(key, context)
                val training = gson.fromJson(json, TrainingEntity::class.java)
                exercises.addAll(training.exercises)
            }
            return exercises
        }

        private fun assetJSONFile(filename: String, context: Context): String {
            val jsonString = context.assets.open(filename).bufferedReader().use {
                it.readText()
            }
            Log.e("json", jsonString)
            return jsonString
        }


    }
}