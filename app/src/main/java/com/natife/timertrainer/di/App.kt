package com.natife.timertrainer.di

import android.preference.PreferenceManager
import com.natife.timertrainer.data.DataBase
import com.natife.timertrainer.data.DataManager
import com.natife.timertrainer.data.DataManagerImpl
import com.natife.timertrainer.data.DatabaseImpl
import com.natife.timertrainer.ui.homefragment.HomeViewModel
import com.natife.timertrainer.ui.main.MainViewModel
import com.natife.timertrainer.ui.timerfragment.TimerViewModel
import com.natife.timertrainer.utils.PreferenceHandler
import com.natife.timertrainer.utils.PreferenceHandlerImpl
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module


private val appModule = module {
    single { DataManagerImpl(get(), androidContext()) as DataManager }
    single { DatabaseImpl() as DataBase }
    single { PreferenceHandlerImpl(get()) as PreferenceHandler }
}

private val preferenceModule = module {
    single { PreferenceManager.getDefaultSharedPreferences(androidContext()) }
}

private val vmModule = module {
    viewModel { HomeViewModel(get()) }
    viewModel { TimerViewModel(get()) }
    viewModel { MainViewModel(get()) }
}

val moduleList = listOf(appModule, vmModule, preferenceModule)


