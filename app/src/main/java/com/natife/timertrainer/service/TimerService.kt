package com.natife.timertrainer.service

import android.app.*
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.media.MediaPlayer
import android.os.*
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.lifecycle.MutableLiveData
import com.natife.timertrainer.R
import com.natife.timertrainer.data.model.ExerciseEntity
import com.natife.timertrainer.data.model.TrainingEntity
import com.natife.timertrainer.ui.main.MainActivity
import com.natife.timertrainer.utils.Utils


class TimerService : Service() {
    companion object {
        private const val NOTIFICATION_CHANNEL_ID = "100"
        private const val NOTIFICATION_INTENT_PAUSE = "pause"
        private const val NOTIFICATION_INTENT_RESUME = "resume"
        const val TRAINING_FINISHED = -1L
    }


    private lateinit var notificationManager: NotificationManager
    private lateinit var notificationBuilder: NotificationCompat.Builder
    private lateinit var timer: CountDownTimer

    var trainingEntity: TrainingEntity? = null
    var currentTimerPosition: Long = 0
    var currentExerciseId: Int = 0
    var currentLoop: Int = 0
    var mutableLiveData: MutableLiveData<Long> = MutableLiveData()
    var serviceCallBack: ServiceCallBack? = null

    var isSkippingPauseEnabled: Boolean = true
    var isAudioEnabled: Boolean = false
    var isVibrationEnabled: Boolean = false
    var isExercise = true
    var isTimerStarted = false
    var isTraining = false

    private lateinit var mediaPlayerExercise: MediaPlayer
    private lateinit var mediaPlayerRest: MediaPlayer

    private var binder = MyBinder()

    override fun onBind(intent: Intent?): IBinder? {
        return binder
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        return START_STICKY
    }

    override fun onCreate() {
        super.onCreate()
        startForeground(100, createWaitNotification())
        mediaPlayerExercise = MediaPlayer.create(this, R.raw.ringtone)
        mediaPlayerRest = MediaPlayer.create(this, R.raw.ding)
        setupNotificationReceiver()
    }

    fun init() {
        cancelTraining()
        loadExercise()
    }

    fun updateViews() {
        trainingEntity?.also { training ->
            training.exercises[currentExerciseId]?.also {
                mutableLiveData.value = currentTimerPosition
                if (isExercise) {
                    serviceCallBack?.onExerciseReady(it, training.exercises.size)
                } else {
                    serviceCallBack?.onRestReady()
                }
            }
        }
    }

    fun updateButton() {
        serviceCallBack?.onNotificationClicked()
    }

    private fun loadExercise() {
        trainingEntity?.also { training ->
            training.exercises[currentExerciseId]?.also {
                currentTimerPosition = it.exerciseTime
            }
            isExercise = true
        }
    }

    private fun loadRest() {
        trainingEntity?.also { training ->
            training.exercises[currentExerciseId]?.also {
                currentTimerPosition = it.restTime
            }
            isExercise = false
        }
    }

    fun startTimer() {
        isTimerStarted = true
        isTraining = true
        try {
            timer.cancel()
        } catch (e: Exception) {
        }
        timer = object : CountDownTimer(currentTimerPosition, 100) {
            override fun onTick(millisUntilFinished: Long) {
                currentTimerPosition = millisUntilFinished
                mutableLiveData.value = millisUntilFinished
                if (millisUntilFinished < 100)
                    currentTimerPosition = -2
                if (millisUntilFinished.rem(1000) > 900) {
                    updateNotification()
                }
            }

            override fun onFinish() {
                isTimerStarted = false
                currentTimerPosition = -1
                mutableLiveData.value = 0
                onTimerFinished()
                updateNotification()
            }
        }.start()
    }

    fun pauseTimer() {
        isTimerStarted = false
        try {
            timer.cancel()
        } catch (e: Exception) {
        }
        updateNotification()
    }

    private var pendingIntent: PendingIntent? = null

    private fun updateNotification() {
        if (isTimerStarted) {
            if (isExercise) {
                notificationBuilder.setContentTitle(getString(R.string.notification_exercise_text))
            } else {
                notificationBuilder.setContentTitle(getString(R.string.notification_rest_text))
            }
        } else {
            notificationBuilder.setContentTitle(getString(R.string.notification_exercise_pause))
        }

        notificationBuilder.mActions.clear()

        if (currentTimerPosition < 0) {
            notificationBuilder.setContentTitle(getString(R.string.notification_exercise_finished))
            notificationBuilder.setContentText(getString(R.string.hurray))
        } else {
            notificationBuilder.setContentText(Utils.getFormattedTime(currentTimerPosition + 1000))
            val intent = Intent()
            pendingIntent = PendingIntent.getBroadcast(this, 1, intent, PendingIntent.FLAG_UPDATE_CURRENT)
            if (isTraining)
                if (isTimerStarted) {
                    intent.action = NOTIFICATION_INTENT_PAUSE
                    pendingIntent = PendingIntent.getBroadcast(this, 1, intent, PendingIntent.FLAG_UPDATE_CURRENT)
                    notificationBuilder.addAction(R.drawable.ic_pause_black_24dp, getString(R.string.notification_exercise_pause), pendingIntent)
                } else {
                    intent.action = NOTIFICATION_INTENT_RESUME
                    pendingIntent = PendingIntent.getBroadcast(this, 1, intent, PendingIntent.FLAG_UPDATE_CURRENT)
                    notificationBuilder.addAction(R.drawable.ic_play_arrow_black_24dp, getString(R.string.resume), pendingIntent)
                }
        }
        notificationManager.notify(100, notificationBuilder.build())
    }


    fun cancelTraining() {
        try {
            timer.cancel()
        } catch (e: Exception) {
        }
        isTimerStarted = false
        isTraining = false
        currentExerciseId = 0
        currentLoop = 0
        currentTimerPosition = 0
    }

    private fun onTimerFinished() {
        makeSound()
        makeVibro()
        trainingEntity?.also {
            if (currentLoop < it.exercises[currentExerciseId]!!.loops - 1) {
                if (!isExercise) {
                    currentLoop++
                    serviceCallBack?.onLoopFinished(it.exercises[currentExerciseId]!!.loops)
                    loadExercise()
                    startTimer()
                } else {
                    loadRest()
                    startTimer()
                }
            } else {
                if (currentExerciseId < it.exercises.size - 1) {
                    currentExerciseId++
                    currentLoop = 0
                    serviceCallBack?.onLoopFinished(it.exercises[currentExerciseId]!!.loops)
                    loadExercise()
                    startTimer()
                    if (isSkippingPauseEnabled) {
                        pauseTimer()
                    }
                } else {
                    serviceCallBack?.onTrainingFinished()
                    isTraining = false
                    return
                }
            }
            updateViews()
        }
    }

    private fun setupNotificationReceiver() {
        val receiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                when (intent.action) {
                    NOTIFICATION_INTENT_PAUSE -> {
                        pauseTimer()
                    }
                    NOTIFICATION_INTENT_RESUME -> {
                        startTimer()
                    }
                }
                updateButton()
                updateNotification()
            }
        }
        val intentFilter = IntentFilter()
        intentFilter.addAction(NOTIFICATION_INTENT_PAUSE)
        intentFilter.addAction(NOTIFICATION_INTENT_RESUME)
        registerReceiver(receiver, intentFilter)
    }

    private fun makeVibro() {
        if (!isVibrationEnabled) return
        val v = getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            if (isExercise)
                v.vibrate(VibrationEffect.createWaveform(longArrayOf(0, 100, 50, 50, 50, 100), -1))
            else
                v.vibrate(VibrationEffect.createOneShot(100, VibrationEffect.DEFAULT_AMPLITUDE))
        } else {
            if (isExercise)
                v.vibrate(longArrayOf(0, 100, 50, 50, 50, 100), -1)
            else
                v.vibrate(100)
        }
    }

    private fun makeSound() {
        if (!isAudioEnabled) return
        if (isExercise) {
            mediaPlayerExercise.start()
        } else {
            mediaPlayerRest.start()
        }
    }

    fun makeWaitNotification() {
        notificationBuilder.setContentTitle(getString(R.string.notification_title))
        notificationBuilder.setContentText(getString(R.string.notification_text_wait))
        notificationManager.notify(100, notificationBuilder.build())
    }

    private fun createWaitNotification(): Notification {
        val resultIntent = Intent(this, MainActivity::class.java)
        resultIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        resultIntent.putExtra("extra", R.id.action_homeFragment_to_timerFragment)

        val resultPendingIntent = PendingIntent.getActivity(this,
                0, resultIntent,
                PendingIntent.FLAG_CANCEL_CURRENT)

        notificationManager = this.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            val notificationChannel = NotificationChannel(NOTIFICATION_CHANNEL_ID, getString(R.string.notification_channel_name), NotificationManager.IMPORTANCE_LOW)
            notificationChannel.enableVibration(false)
            notificationManager.createNotificationChannel(notificationChannel)
        }

        notificationBuilder = NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID)
        notificationBuilder.setSmallIcon(R.drawable.natife_logo)
                .setContentTitle(getString(R.string.notification_title))
                .setContentText(getString(R.string.notification_text_wait))
                .setAutoCancel(false)
                .setContentIntent(resultPendingIntent)
        return notificationBuilder.build()
    }

    internal inner class MyBinder : Binder() {
        val service: TimerService
            get() = this@TimerService
    }

    interface ServiceCallBack {
        fun onExerciseReady(exerciseEntity: ExerciseEntity, exerciseQuantity: Int)
        fun onRestReady()
        fun onLoopFinished(loops: Int)
        fun onTrainingFinished()
        fun onNotificationClicked()
    }
}