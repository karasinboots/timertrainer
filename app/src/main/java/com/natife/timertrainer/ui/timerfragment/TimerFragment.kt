package com.natife.timertrainer.ui.timerfragment

import android.app.AlertDialog
import android.content.Intent
import android.graphics.Rect
import android.media.AudioManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.TouchDelegate
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.CompoundButton
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.google.android.gms.ads.AdView
import com.natife.timertrainer.R
import com.natife.timertrainer.data.model.ExerciseEntity
import com.natife.timertrainer.service.TimerService
import com.natife.timertrainer.ui.main.MainViewModel
import com.natife.timertrainer.utils.Utils
import kotlinx.android.synthetic.main.fragment_timer.*
import kotlinx.android.synthetic.main.toolbar.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel


/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [TimerFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [TimerFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class TimerFragment : Fragment(), TimerService.ServiceCallBack {
    private val timerViewModel by viewModel<TimerViewModel>()
    private val mainViewModel by sharedViewModel<MainViewModel>()

    private var timerService: TimerService? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        lifecycle.addObserver(timerViewModel)
        timerService = mainViewModel.timerService
        button_start_pause.setOnClickListener {
            timerService?.also {
                if (!it.isTimerStarted) {
                    it.startTimer()
                    button_start_pause.text = getString(R.string.pause)
                } else {
                    it.pauseTimer()
                    button_start_pause.text = getString(R.string.resume)
                }
            }
        }

        toolbar_exit_right.visibility = View.VISIBLE
        toolbar_exit_right.setOnClickListener {
            activity!!.stopService(Intent(activity, TimerService::class.java))
            System.exit(0)
        }

        activity?.also {
            it.volumeControlStream = AudioManager.STREAM_MUSIC
            it.findViewById<AdView>(R.id.adView).visibility = View.VISIBLE
        }

        toolbar_back_button.setOnClickListener {
            findNavController().popBackStack()
        }

        toolbar_title.text = timerService?.trainingEntity?.name
        timerService?.also {
            it.serviceCallBack = this@TimerFragment
            (timerService?.mutableLiveData as LiveData<Long>).observe(this@TimerFragment,
                    Observer<Long> {
                        if (it > 0) {
                            onTimerTick(it)
                        } else {
                            timer_progress.progress = timer_progress.max
                        }
                    })
            timerService?.trainingEntity?.exercises?.size?.also {
                training_progress.max = it
            }
            it.updateViews()
            if (it.currentTimerPosition == TimerService.TRAINING_FINISHED) {
                startFinishProcedure()
            }
        }
        prepareSettings()
    }

    private fun prepareSettings() {
        timerService?.isSkippingPauseEnabled = timerViewModel.isSkippingPauseEnabled()
        timerService?.isAudioEnabled = timerViewModel.isAudioEnabled()
        timerService?.isVibrationEnabled = timerViewModel.isVibrationEnabled()

        sound_button.isChecked = timerViewModel.isAudioEnabled()
        vibro_button.isChecked = timerViewModel.isVibrationEnabled()
        pause_skip_button.isChecked = timerViewModel.isSkippingPauseEnabled()

        expandViewHitArea(sound_wrapper, sound_button)
        expandViewHitArea(vibro_wrapper, vibro_button)
        expandViewHitArea(pause_wrapper, pause_skip_button)

        sound_button.setOnCheckedChangeListener { _: CompoundButton, b: Boolean ->
            timerViewModel.setAudioEnabled(b)
            timerService?.isAudioEnabled = timerViewModel.isAudioEnabled()
        }

        vibro_button.setOnCheckedChangeListener { _: CompoundButton, b: Boolean ->
            timerViewModel.setVibrationEnabled(b)
            timerService?.isVibrationEnabled = timerViewModel.isVibrationEnabled()
        }

        pause_skip_button.setOnCheckedChangeListener { _: CompoundButton, b: Boolean ->
            timerViewModel.setSkipPauseEnabled(b)
            timerService?.isSkippingPauseEnabled = timerViewModel.isSkippingPauseEnabled()
        }
    }

    private fun expandViewHitArea(parent: View, child: View) {
        parent.post {
            val parentRect = Rect()
            val childRect = Rect()
            parent.getHitRect(parentRect)
            child.getHitRect(childRect)

            childRect.left = 0
            childRect.top = 0
            childRect.right = parentRect.width()
            childRect.bottom = parentRect.height()

            parent.touchDelegate = TouchDelegate(childRect, child)
        }
    }

    private fun onTimerTick(millis: Long) {
        timerService?.also { service ->
            service.trainingEntity?.also {
                it.exercises[service.currentExerciseId]?.also {
                    if (!service.isExercise) {
                        timer_progress.progress = Utils.getProgressInPercents(it.restTime, millis)
                    } else {
                        timer_progress.progress = Utils.getProgressInPercents(it.exerciseTime, millis)
                    }
                }
            }
            text_timer.text = Utils.getFormattedTime(millis + 1000)
        }
    }

    override fun onExerciseReady(exerciseEntity: ExerciseEntity, exerciseQuantity: Int) {
        timerService?.also {
            name_text.text = exerciseEntity.name
            text_timer.text = Utils.getFormattedTime(it.currentTimerPosition)
            setStartButtonText()
            description_text.text = exerciseEntity.description
            sub_timer_text.text = getString(R.string.current_exercise, it.currentExerciseId + 1, exerciseQuantity)
            training_progress.progress = it.currentExerciseId
            loop_count.text = getString(R.string.current_exercise, it.currentLoop + 1,
                    exerciseEntity.loops)
        }
    }

    override fun onRestReady() {
        timerService?.also {
            text_timer.text = Utils.getFormattedTime(it.currentTimerPosition)
            name_text.text = getString(R.string.rest)
            setStartButtonText()
            description_text.text = getString(R.string.rest_description)
        }
    }

    override fun onNotificationClicked() {
        setStartButtonText()
    }

    private fun setStartButtonText() {
        timerService?.also {
            when {
                it.isTimerStarted -> button_start_pause.text = getString(R.string.pause)
                it.isTraining -> button_start_pause.text = getString(R.string.resume)
                else -> button_start_pause.text = getString(R.string.start)
            }
        }
    }

    override fun onLoopFinished(loops: Int) {
        timer_progress.progress = timer_progress.max
        timerService?.also {
            loop_count.text = getString(R.string.current_exercise, it.currentLoop + 1,
                    loops)
        }
    }

    override fun onTrainingFinished() {
        startFinishProcedure()
    }

    var dialog: AlertDialog? = null

    private fun startFinishProcedure() {
        timer_progress.progress = timer_progress.max
        training_progress.progress = training_progress.max
        text_timer.text = getString(R.string.zero)
        val builder = AlertDialog.Builder(context)
        val inflater = this.layoutInflater
        val dialogView = inflater.inflate(R.layout.finish_dialog, null)
        builder.setView(dialogView)
        builder.setCancelable(false)
        dialog = builder.create()
        dialog?.show()
        dialogView.findViewById<Button>(R.id.to_home).setOnClickListener {
            timerService?.cancelTraining()
            findNavController().popBackStack()
            dialog?.dismiss()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_timer, container, false)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        timerService?.serviceCallBack = null
        activity?.also {
            it.volumeControlStream = AudioManager.USE_DEFAULT_STREAM_TYPE
            it.findViewById<AdView>(R.id.adView).visibility = View.GONE
        }
    }

}
